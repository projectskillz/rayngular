'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    test: {
      files: ["test/**/*.js"]
    },

    lint: {
      files: ["grunt.js", "app/js/**/*.js", "test/e2e/**/*.js", "test/unit/**/*.js"]
    },

    watch: {
      files: "<config:lint.files>",
      tasks: "default"
    },

    jshint: {
      options: {
        curly: true,
        eqeqeq: true,
        immed: true,
        latedef: true,
        newcap: true,
        noarg: true,
        sub: true,
        undef: true,
        boss: true,
        eqnull: true,
        node: true,
        es5: true,
        globalstrict: true
      },
      globals: {
        it: true,
        beforeEach: true,
        inject: true,
        describe: true,
        element: true,
        expect: true,
        browser: true,
        angular: true,
        MyCtrl1: true,
        MyCtrl2: true
      }
    }
  });

  // Default task.
  grunt.registerTask("default", "lint test");

  grunt.registerTask('test', 'run tests', function () {
    var done = this.async();
    grunt.utils.spawn({
      cmd: process.platform === 'win32' ? 'scripts/test.bat' : 'scripts/test.sh'
    }, function (error, result, code) {
      if (error) {
        grunt.warn("Make sure the testacular server is online: run `grunt server`.\n" +
          "Also make sure you have a browser open to http://localhost:8080/.\n" +
          error.stdout + error.stderr);
        //the testacular runner somehow modifies the files if it errors(??).
        //this causes grunt's watch task to re-fire itself constantly,
        //unless we wait for a sec
        setTimeout(done, 1000);
      } else {
        grunt.log.write(result.stdout);
        done();
      }
    });
  });
};